import os
from bson import ObjectId
from bson.errors import InvalidId
from typing import List

from fastapi import Depends, FastAPI, Body, HTTPException
from fastapi.encoders import jsonable_encoder

from app.db.mongodb import show_db, season_db, episode_db
from app.db.auth import get_user
from app.models import User, ResponseModel, Show, ShowInDB, ShowDetail,\
        Episode, EpisodeInDB, Season, SeasonInDB, SeasonDetail


app = FastAPI(
    title='TT Catalog',
    version='0.1.2',
    debug=os.getenv("DEBUG", False)
)


async def convert_object_id(id):
    try:
        return ObjectId(id)
    except InvalidId:
        raise HTTPException(
                status_code=400,
                detail="id field is INVALID"
                )


async def get_show_data(show_id: str):
    id = await convert_object_id(show_id)
    if (show := await show_db.find_one({"_id": ObjectId(id)})) is not None:
        return show
    raise HTTPException(status_code=404)


async def get_season_data(show_id: str, number: int):
    show = await get_show_data(show_id)
    if (season := await season_db.find_one({'show_id': show['_id'], 'number': number})) is not None:
        return season
    raise HTTPException(status_code=404)


@app.post('/', status_code=201,
          response_model=ShowInDB,
          include_in_schema=False)
async def create_show(show: Show, user: User = Depends(get_user)):
    show = jsonable_encoder(show)
    new_show = await show_db.insert_one(show)
    created_show = await show_db.find_one({'_id': new_show.inserted_id})
    return created_show


@app.get('/', status_code=200, response_model=List[ShowInDB])
async def list_shows():
    """
    Get list of shows. It just gives **id**, **name**, **year** and **rating** informations.
    """
    shows = await show_db.find().to_list(100)
    return shows


@app.get('/{show_id}/', status_code=200, response_model=ShowDetail)
async def get_show_details(show_id: str):
    """
    Get show details. Beside **id**, **name**, **year**, and **rating**, it also gives **seasons**self.

    Season list only contains **number** and **id** for of the season.
    """
    id = await convert_object_id(show_id)
    show = await show_db.aggregate([
        {"$match": {'_id': id}},
        {"$lookup": {
            'from': 'season',
            'localField': '_id',
            'foreignField': 'show_id',
            'pipeline': [
                {'$sort': {'number': 1}}
            ],
            'as': 'seasons',
        }},
    ]).to_list(1)
    if len(show) == 1:
        return show[0]
    raise HTTPException(status_code=404)


@app.post('/{show_id}/season/',
          status_code=201, response_model=SeasonInDB,
          include_in_schema=False)
async def create_season(show_id: str, season: Season, user: User = Depends(get_user)):
    show = await get_show_data(show_id)
    season = jsonable_encoder(season)
    season.update({'show_id': show['_id']})
    new_season = await season_db.update_one(season, {'$setOnInsert': season}, upsert=True)
    if getattr(new_season, 'upserted_id'):
        created_season = await season_db.find_one({'_id': new_season.upserted_id})
        return created_season
    raise HTTPException(status_code=200, detail='Already Exists')


@app.get('/{show_id}/season/{number}/', status_code=200, response_model=SeasonDetail)
async def get_season_details(show_id: str, number: int):
    """
    Get season information with **episodes** list.

    Episodes contains **number** and **name** informations.
    """
    season = await get_season_data(show_id, number)
    season_in_db = await season_db.aggregate([
        {'$match': {'_id': season['_id']}},
        {'$lookup': {
            'from': 'episode',
            'localField': '_id',
            'foreignField': 'season_id',
            'pipeline': [{'$sort': {'number': 1}}],
            'as': 'episodes'
        }},
    ]).to_list(1)
    return season_in_db[0]


@app.post('/{show_id}/season/{number}/episode',
          status_code=201, response_model=EpisodeInDB,
          include_in_schema=False)
async def create_episode(show_id: str, number: int, episode: Episode, user: User = Depends(get_user)):
    season = await get_season_data(show_id, number)
    episode = jsonable_encoder(episode)
    episode.update({'season_id': season['_id']})
    new_episode = await episode_db.update_one(episode, {'$setOnInsert': episode}, upsert=True)
    if getattr(new_episode, 'upserted_id'):
        created_episode = await episode_db.find_one({'_id': new_episode.upserted_id})
        return created_episode
    raise HTTPException(status_code=200, detail='Already Exists')
