from fastapi import Depends, HTTPException
from fastapi.security import HTTPBasic, HTTPBasicCredentials

from app.db.mongodb import user_db
from app.models import User, UserInDB


security = HTTPBasic()


async def get_user(credentials: HTTPBasicCredentials = Depends(security)):
    if (db_user := await user_db.find_one({'username': credentials.username})) is not None:
        db_user = UserInDB(**db_user)
        if db_user.check_password(credentials.password):
            return User(**db_user.dict())

    raise HTTPException(
        status_code=404,
        detail='Invalid auth information.'
    )


async def check_user_permissions(user: User, perm: str):
    if perm in user.permissions or 'all' in user.permissions:
        return True
    raise HTTPException(
        status_code=301,
        detail='Permission Denied'
    )
